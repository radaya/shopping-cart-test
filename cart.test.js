const cart = require('./cart.js');

test('A cart with 3 ult_small and 1 ult_large should have a total of $94.70', () => {
  const shoppingCart = new cart.ShoppingCart(cart.pricingRules)
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_large')
  expect(shoppingCart.calc()).toEqual('$94.70');
});

test('A cart with 2 ult_small and 4 ult_large should have a total of $209.40', () => {
  const shoppingCart = new cart.ShoppingCart(cart.pricingRules)
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_large')
  expect(shoppingCart.calc()).toEqual('$209.40');
});

test('A cart with 1 ult_small and 2 ult_medium should have a total of $84.70', () => {
  const shoppingCart = new cart.ShoppingCart(cart.pricingRules)
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_medium')
  shoppingCart.add('ult_medium')
  expect(shoppingCart.calc()).toEqual('$84.70');
});

test('No order: A cart with 2 ult_small and 4 ult_large should have a total of $209.40', () => {
  const shoppingCart = new cart.ShoppingCart(cart.pricingRules)
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_small')
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_large')
  shoppingCart.add('ult_small')
  expect(shoppingCart.calc()).toEqual('$209.40');
});

test('A cart with a promode code, 1 ult_small and 1 1GB-data-pack should have a total of $31.32', () => {
  const shoppingCart = new cart.ShoppingCart(cart.pricingRules)
  shoppingCart.setPromoCode(cart.PROMO_CODE)
  shoppingCart.add('ult_small')
  shoppingCart.add('1gb')
  expect(shoppingCart.calc()).toEqual('$31.32');
});
