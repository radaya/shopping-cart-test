const priceMap = {
  'ult_small': {
    'name': 'Ulimited 1GB', 
    'price': 24.90,
  },
  'ult_medium': {
    'name': 'Ulimited 2GB', 
    'price': 29.90,
  },
  'ult_large': {
    'name': 'Ulimited 5GB', 
    'price': 44.90,
  },
  '1gb': {
    'name': '1 GB Data-pack', 
    'price': 9.90,
  }
}

const pricingRules = {
  'three_for_two_1GB': {
    'price': 16.6,
  },
  'bulk_5GB': {
    'price': 39.90
  }
} 

const CURRENCY = '$'
const PROMO_CODE = 'I<3AMAYSIM'
const DISCOUNT = 10

class ShoppingCart {
  constructor(pricingRules) {
    this.items = []
    this.promoCode = false
    this.pricingRules = pricingRules
  }

  setPromoCode (promoCode) {
    if (promoCode === PROMO_CODE) {
      this.promoCode = true
    }
  }

  add (item) {
    this.items.push(item)
  }

  calc () {
    const cartMap = {}

    for (const item of this.items) {
      cartMap[item] = cartMap[item] + 1 || 1
    }

    let partial = 0
    for (const item in cartMap) {
      if (item === 'ult_small' && cartMap[item] === 3) {
        partial += this.pricingRules.three_for_two_1GB.price * cartMap.ult_small
      } else if (item === 'ult_large' && cartMap[item] > 3) {
        partial += this.pricingRules.bulk_5GB.price * cartMap.ult_large
      } else {
        partial += priceMap[item].price * cartMap[item]
      }
    }

    const total = this.promoCode
      ? partial - ((DISCOUNT / 100) * partial)
      : partial

    return `${CURRENCY}${total.toFixed(2)}` 
  }
}

module.exports = {
  ShoppingCart,
  pricingRules,
  PROMO_CODE,
}